# EC-Orange 開発者ガイド

## はじめに

このドキュメントは EC-Orange をカスタマイズしてアプリケーションを開発する際に守っていただきたい下記についてのガイドを示します。  

- クラス設計
- 利用する Laravel フレームワークの機能

ガイドに沿って開発していただくことで、はじめて EC-Orange を用いた開発を行う方も効率よくクラス設計が行え、一定のコード品質になることを目指します。  
なお、下記についてはこのドキュメントの範囲外としておりますので、別途資料を参照してください。

- コーティングルール
    - Laraval のゴーディングルールに準拠してください
        - 参考）[Laravel Coding Style](https://laravel.com/docs/master/contributions#coding-style)

## 目次

- [はじめに](#はじめに)
- [対象バージョン](#対象バージョン)
- [対象読者](#対象読者)
- [設定項目](#設定項目)
- [実装パターン](#実装パターン)
    - [Controller](#Controller)
    - [Services](#Services)
    - [Repositories](#Repositories)
    - [Eloquent](#Eloquent)
- [バッチ処理](#バッチ処理)
- [外部連携の実装](#外部連携の実装)
- [EC-Orangeの標準処理を拡張する](#EC-Orangeの標準処理を拡張する)
    - [Controllerの拡張](#Controllerの拡張)
    - [Servicesの拡張](#Servicesの拡張)
    - [Repositoriesの拡張](#Repositoriesの拡張)
    - [Eloquent(Model)の拡張](#Eloquent(Model)の拡張)
- [Viewの実装](#Viewの実装)
    - [アノテーションによるコードの再利用](#アノテーションによるコードの再利用)
    - [カスタマイズ対象ファイル](#カスタマイズ対象ファイル)
    - [null制御](#null制御)
- [購入フローのセッション利用](#購入フローのセッション利用)
- [エラーハンドリング](#エラーハンドリング)
- [ロギングポリシー](#ロギングポリシー)
- [ユニットテストの実装](#ユニットテストの実装)
- [マイグレーションの活用（Migration）](#マイグレーションの活用（Migration）)
- [Tips](#Tips)
    - [実行されるSQLの確認](#実行されるSQLの確認)

## 対象バージョン

- EC-Orange 4.3
    - Laravel 6系
    - PHP 7.4
    - MySQL 8.0

## 対象読者

このドキュメントが想定している読者は下記の方々です。

- EC-Orange をカスタマイズする開発者の方
- EC-Orange ベースのシステムを改修する保守担当者の方

## 設定項目

稼働環境毎の設定は .env ファイルにて行います。
特に重要な設定項目について説明します。

- APP_ENV
    - アプリケーションの稼働環境を指定します。
- APP_DEBUG
    - true に設定するとサーバーエラー発生時に画面上にコールスタックが表示されます。開発環境、テスト環境で利用してください。
- CONST_SYSTEM_PACKAGE
    - MALL or SHOP が指定可能です。変更にはデータベースの再構築が必要です。
        - MALL : マルチテナント、いわゆるモール型 EC として起動します。
        - SHOP : 単一店舗による EC として起動します。
- CONST_SYSTEM_COMMERCE_TYPE
    - BTOC or BTOB が指定可能です。変更にはデータベースの再構築が必要です。
        - BTOC : コンシューマー向けの EC として起動します。
        - BTOB : ビジネス向けの EC として起動します。
- CONST_SYSTEM_STARTUP_MODE
    - EC or STORE or OMNI が指定可能です。変更にはデータベースの再構築が必要です。
        - EC : EC のみを運営するモードとして起動します。
        - STORE : 店舗のみを運営するモードとして起動します。
        - OMNI : EC および店舗双方を運営するモードとして起動します。
- CONST_SYSTEM_SHOP_TYPE_MULTI_DOMAIN
    - true に設定すると MALL 毎に異なるドメインを設定する「マルチドメイン」での運用が可能です。CONST_SYSTEM_PACKAGE が "MALL" の場合のみ指定できます。通常、店舗の識別は URLパスにより識別しますが、マルチドメインの場合はカスタム httpヘッダ "X-Header-Shop-Key" にて識別します。この場合、別途 Webサーバの設定が必要となります。変更にはデータベースの再構築が必要です。

その他の設定項目については提供している下記ファイルを参照してください。

- 【s-cubism】標準_環境変数情報.xlsx


## 実装パターン

EC-Orange のクラス設計には レイヤードアーキテクチャパターンを採用しています。従ってカスタマイズによるクラス追加を行う場合もこのパターンに沿ったクラス設計を行ってください。  
また、各レイヤーには依存関係の方向が決まっており、これを守ることで各クラスの責務が明確化し疎結合を保つことができます。 Services から Services を参照したり、Repositories から Services の参照などはしないようにします。

__各レイヤーが参照できる方向__
```mermaid
graph TD;
    Controller-->Services;
    Services-->Repositories;
    Repositories-->Eloquent;
```

ここでは定義されている各レイヤーについて説明します。

### Controller

プレゼンテーションレイヤー。ユーザーに対するシステムの振舞いを実装し、具体的には以下の制御が含まれます。業務ロジックは実装しません。

- 業務ロジック(Services)の呼び出し
- 例外に基づくレスポンス(View or JSON)のハンドリング
- 業務ロジックの結果に基づくレスポンス(View or JSON)のハンドリング

### Services

アプリケーションレイヤー。状態を保持しない（ステートレスな）処理を実装し、ユースケースを実現します。トランザクション処理を実装する Repository の呼び出しと、それ以外の処理（画像の操作または外部接続など）を組み合わせた処理を行います。  
トランザクションの制御はこのレイヤーでは行わず、Repositories で処理します。

__例）__ /app/Services/Webmaster/Base/ProductCategoryServiceBase.php

製品カテゴリ情報の削除（トランザクション）呼び出しと関連する画像の削除を行います。

``` php
protected final function deleteBase(string $productCategoryId)
{
    $productCategoryRepository = new ProductCategoryRepository();
    $result = $productCategoryRepository->get($productCategoryId);
    $saveFilePath = Config('const.SYSTEM.SAVE_IMAGE_FOLDER.PRODUCT_CATEGORY');
    ImageUtil::deleteImage($saveFilePath, $result['product_category_image']);
    return $productCategoryRepository->delete($productCategoryId);
}
```

### Repositories

ビジネスロジックレイヤー。データベースのトランザクション単位でクラス化します。よってデータベース・テーブルに対して1対1である必要はありません。

__例）__ /app/Repositories/Webmaster/Base/CouponRepositoryBase.php

クーポン情報と関連する顧客クーポン情報の削除を同一トランザクションとして処理しています

``` php
protected final function deleteBase(string $couponId)
{
    DB::transaction(function() use ($couponId) {
        $coupon = new Coupon();
        if (is_null($coupon->find($couponId))) {
            return;
        }
        $coupon
            ->find($couponId)
            ->delete();
        if ($coupon->getTranslationTale() === true) {
            $couponTranslation = new CouponTranslation();
            $couponTranslation
                ->find($couponId)
                ->delete();
        }
        $customerCoupon = new CustomerCoupon();
        $customerCoupon
            ->where('coupon_id', $couponId)
            ->delete();
    });
    return $couponId;
}
```

### Eloquent

インフラストラクチャーレイヤー。Laravel の ORM マッパー "Eloquent" を利用しモデルクラスを実装します。モデルクラスはテーブル毎に1つのクラスを作成します。  
なお、データベースの操作に関する CRUD の実装は後述の [Repositories](#Repositories) レイヤーで実装し、モデルには実装しません。

## バッチ処理

バッチ処理は Laravel のタスクスケジュール機能を使って実装します。バッチ処理の実態は Command クラスとして実装し、/app/Console/Commands ネームスペース内に配置します。  
クラスの責務は [Controller](#Controller) と同様とし、Services レイヤーのみに依存するようにします。

- 参考
    - [Lraval タスクスケジュール](https://readouble.com/laravel/6.x/ja/scheduling.html)

## 外部連携の実装

対象案外部の API を呼び出して連携を行う場合はフロント、管理画面双方での利用を想定し、"/common/Services" ネームスペース内に連携先ごとにクラスを作成します。

__参考__
- /common/Services/Settlement/Gmo/Base/GmoConnectionCreditServiceBase.php
- /common/Services/Settlement/Gmo/GmoConnectionCreditService.php

## EC-Orangeの標準処理を拡張する

EC-Orange の全てのクラスは各レイヤー直下と、そのレイヤー内の Base ネームスペース内にペアで存在(以降、Baseクラス)し、パッケージとして提供する標準関数は全て Baseクラスに定義されており、関数名にも接尾辞 "Base" が付与されています(以後、Base関数)。  
上記を踏まえ案件によるカスタマイズは各レイヤー直下のクラスをカスタマイズして実現します。そうすることで標準パッケージの改修を受け入れやすくし、また問題発生時の切り分けを行いやすくします。

ここでは各レイヤーにおけるカスタマイズの方法をガイドします。

### Controllerの拡張

Controller のBase関数をカスタマイズするパターンは以下の3つに分類されます。

#### 事前処理を追加する

追加する事前処理を実装した後に Base関数を呼び出します。

__例）/app/Http/Controllers/Webmaster/CustomerController.php__
``` php
public function getList(Request $request)
{
    /*
     * ここに事前処理を実装
     */

    // Base関数呼び出し
    return parent::getListBase($request);
}
```

#### 事後処理を追加する

Base関数を呼び出してから後処理として追加する処理を実装します。

__例）/app/Http/Controllers/Webmaster/CustomerController.php__
``` php
public function getList(Request $request)
{
    // Base関数呼び出し
    $view = parent::getListBase($request);

    /*
     * ここに事後処理を実装
     */

    return $view;
}
```

#### Base関数の処理そのものを変更する

Base関数は利用できませんので、対応する Base関数を参考に関数全体を実装します。Base関数は呼び出さないでください。

### Servicesの拡張

考え方は [Controllerの拡張](#Controllerの拡張) と同様です。

### Repositoriesの拡張

考え方は [Controllerの拡張](#Controllerの拡張) と同様です。

### Eloquent(Model)の拡張

#### カラムフィールドの追加

PHP ではクラス変数として宣言された変数の値をサブクラスで変更することはできません。そのため、カラムに対応するフィールドを含めた全フィールドを変数 $fillable に再定義します。

__例）/app/Eloquent/Customer.php__
``` php
class Customer extends CustomerBase
{
    protected $fillable = [
        /* Baseクラスに定義されている全てのカラムを再定義し */
        'customer_id',
        'shop_id',
        'customer_login_id',
        /* ... */
        // 追加するカラムも含める
        'customer_tel04'
    ];
}
```

カラムに関連した以下のクラス変数についても同様です。

- $searchable
- $searchableMultiple
- $csvColumn
- $hidden

## Viewの実装

### アノテーションによるコードの再利用

Laravel標準のテンプレートエンジン "Blade" を利用します。Blade の機能であるアノテーションを積極的に使いコードの再利用を行います。

- 参考
    - [Laravel 6.x Bladeテンプレート](https://readouble.com/laravel/6.x/ja/blade.html)

### カスタマイズ対象ファイル

カスタマイズは下記ディレクトリ配下のファイルに対して行います。

```
/resources/views/website/templates/apparel/ja/pc
```

上記ファイルがテンプレートファイルとして利用され、管理画面上での店舗生成時に各店舗ディレクトリ配下にコピーされます。  
そのため、開発者各自の環境では blade ファイル更新後に各店舗へコピーする必要があります。

### null制御

表示する値の null 制御（null だったら空文字を表示する等）は PHP7 より導入された「Null 合体演算子」を利用して View 側で実装します。

__例）"customer_id" が null だったら空文字を表示__
``` html
<label for="customer_id">{{ $customer['customer_id'] ?? '' }}</label>
```

## 購入フローのセッション利用

顧客の購入フローは大きく下記2つのセクションに分かれ、それぞれの情報がセッション管理されています。
- カート情報の編集
- 購入情報の編集

カート情報をインプットとして購入情報が作成されるため、カート情報と購入情報は密に連携します。設計段階より相互に意識をする必要があります。

### カート情報の構造

カート情報の構造概要は下記となっています。
```
{
    商品SKUID: {
        商品情報の各種プロパティ,
        ...
    }
}
```

__出力例）__
```
(
  '0000000000000003' => 
  array (
    'product_id' => '0000000000000002',
    'product_name' => '木のお皿',
    'product_sales_quantity' => 10,
    'product_sales_unlimited' => 0,
    'product_delivery_day' => 0,
    'product_tax_rate' => 10.0,
    'product_tax_type' => 1,
    'product_sku_id' => '0000000000000003',
    'product_code' => 'z001',
    'product_sku_code' => 'dishs',
    'product_sku_article_code' => '',
    'product_sku_company_code' => '',
    'product_sku_warehouse_code' => '',
    'product_sku_maker_code' => '',
    'product_sku_price_cost' => NULL,
    'product_sku_price_list' => NULL,
    'product_sku_price_selling' => '1000.00',
    'product_sku_price_selling_original' => '1000.00',
    'product_sku_stock_quantity' => 45,
    'product_sku_stock_unlimited' => 0,
    'product_subscribe_use' => 0,
    'product_type' => 1,
    'product_gift_flg' => 0,
    'product_gift_id' => '',
    'option_id' => '',
    'product_sale_id' => NULL,
    'product_sale_type' => NULL,
    'product_sale_value' => NULL,
    'product_sets' => 
    array (
    ),
    'product_bundles' => 
    array (
    ),
    'brand_id' => '',
    'brand_name' => '',
    'department_id' => '',
    'department_code' => '',
    'department_name' => '',
    'quantity' => 
    array (
      'order' => '1',
      'subscribe' => 0,
    ),
    'product_image' => 
    array (
      1 => 
      array (
        'product_image_name' => 'product_image_name_list.jpg',
        'product_image_alt' => '',
      ),
      2 => 
      array (
        'product_image_name' => '',
        'product_image_alt' => '',
      ),
      3 => 
      array (
        'product_image_name' => '',
        'product_image_alt' => '',
      ),
    ),
    'product_sku_variation' => 
    array (
      1 => 
      array (
        'variation_category_id' => '0000000000000002',
        'variation_id' => '0000000000000004',
        'variation_category_name' => 'サイズ',
        'variation_name' => 'S',
      ),
    ),
  ),
)  
```
### 購入情報の構造

購入情報の構造概要は下記となっています。
```
{
    'order': {
        オーダー情報各種プロパティ,
    },
    'order_customer': {
        注文者情報各種プロパティ,
    },
    'order_item': {
        注文商品情報各種プロパティ,
    },
    'order_shipping': {
        注文配送情報各種プロパティ,
    },
    'order_payment': {
        注文支払情報各種プロパティ,
    },
    'order_option': {
        注文オプション情報各種プロパティ,
    },
    'order_settlement': {
        注文決済情報各種プロパティ,
    },
    'order_point': {
        注文ポイント情報各種プロパティ,
    },
    'order_coupon': {
        注文クーポン情報各種プロパティ,
    },
    'area_display': {
        画面表示制御各種プロパティ,
    },
    'subscribe': {
        定期注文各種オーダ情報,
    },

}
```

__出力例）__
```
(
  'order' => 
  array (
    'order' => 
    array (
      'order_items_taxed' => 1100.0,
      'order_items_taxless' => 1000.0,
      'order_items_tax' => 
      array (
        1 => 
        array (
          10 => 100.0,
        ),
      ),
      'order_charge_taxless' => 455.0,
      'order_charge_taxed' => 500.0,
      'order_charge_tax' => 45.0,
      'order_shipping_delivery_taxless' => 455.0,
      'order_shipping_delivery_taxed' => 500.0,
      'order_shipping_delivery_tax' => 45.0,
      'order_shipping_charge_taxless' => 728.0,
      'order_shipping_charge_taxed' => 800.0,
      'order_shipping_charge_tax' => 72.0,
      'order_option_taxless' => 0,
      'order_option_taxed' => 0,
      'order_option_tax' => 0,
      'order_total_paid_taxless' => 2638.0,
      'order_total_paid_taxed' => 2900.0,
      'order_total_paid_tax' => 262.0,
      'order_total_settlement_paid' => 2900.0,
      'order_comment' => '',
    ),
    'order_customer' => 
    array (
      'customer_id' => '0000000000000002',
      'order_customer_email' => 'isao.tanji+02@s-cubism.jp',
      'order_customer_last_name' => 'カスタマ―',
      'order_customer_first_name' => '１',
      'order_customer_last_name_kana' => 'カスタマ',
      'order_customer_first_name_kana' => 'イチ',
      'order_customer_zip01' => '213',
      'order_customer_zip02' => '0011',
      'order_customer_pref' => 13,
      'order_customer_city' => '港区',
      'order_customer_locality' => '芝',
      'order_customer_street' => '大門',
      'order_customer_other' => '',
      'order_customer_tel01' => '03',
      'order_customer_tel02' => '0000',
      'order_customer_tel03' => '0000',
      'order_customer_fax01' => '',
      'order_customer_fax02' => '',
      'order_customer_fax03' => '',
    ),
    'order_item' => 
    array (
      '0000000000000003' => 
      array (
        'product_id' => '0000000000000002',
        'product_name' => '木のお皿',
        'product_sales_quantity' => 10,
        'product_sales_unlimited' => 0,
        'product_delivery_day' => 0,
        'product_tax_rate' => 10.0,
        'product_tax_type' => 1,
        'product_sku_id' => '0000000000000003',
        'product_code' => 'z001',
        'product_sku_code' => 'dishs',
        'product_sku_article_code' => '',
        'product_sku_company_code' => '',
        'product_sku_warehouse_code' => '',
        'product_sku_maker_code' => '',
        'product_sku_price_cost' => NULL,
        'product_sku_price_list' => NULL,
        'product_sku_price_selling' => '1000.00',
        'product_sku_price_selling_original' => '1000.00',
        'product_sku_stock_quantity' => 45,
        'product_sku_stock_unlimited' => 0,
        'product_subscribe_use' => 0,
        'product_type' => 1,
        'product_gift_flg' => 0,
        'product_gift_id' => '',
        'option_id' => '',
        'product_sale_id' => NULL,
        'product_sale_type' => NULL,
        'product_sale_value' => NULL,
        'product_sets' => 
        array (
        ),
        'product_bundles' => 
        array (
        ),
        'brand_id' => '',
        'brand_name' => '',
        'department_id' => '',
        'department_code' => '',
        'department_name' => '',
        'quantity' => 
        array (
          'order' => '1',
          'subscribe' => 0,
        ),
        'product_image' => 
        array (
          1 => 
          array (
            'product_image_name' => 'product_image_name_list.jpg',
            'product_image_alt' => '',
          ),
          2 => 
          array (
            'product_image_name' => '',
            'product_image_alt' => '',
          ),
          3 => 
          array (
            'product_image_name' => '',
            'product_image_alt' => '',
          ),
        ),
        'product_sku_variation' => 
        array (
          1 => 
          array (
            'variation_category_id' => '0000000000000002',
            'variation_id' => '0000000000000004',
            'variation_category_name' => 'サイズ',
            'variation_name' => 'S',
          ),
        ),
      ),
    ),
    'order_shipping' => 
    array (
      0 => 
      array (
        'order_shipping_id' => '0',
        'order_shipping_last_name' => 'カスタマ―',
        'order_shipping_first_name' => '１',
        'order_shipping_last_name_kana' => 'カスタマ',
        'order_shipping_first_name_kana' => 'イチ',
        'order_shipping_zip01' => '213',
        'order_shipping_zip02' => '0011',
        'order_shipping_pref' => 13,
        'order_shipping_city' => '港区',
        'order_shipping_locality' => '芝',
        'order_shipping_street' => '大門',
        'order_shipping_other' => '',
        'order_shipping_tel01' => '03',
        'order_shipping_tel02' => '0000',
        'order_shipping_tel03' => '0000',
        'order_shipping_fax01' => '',
        'order_shipping_fax02' => '',
        'order_shipping_fax03' => '',
        'order_shipping_item' => 
        array (
          '0000000000000003' => 
          array (
            'product_id' => '0000000000000002',
            'product_sku_id' => '0000000000000003',
            'product_code' => 'z001',
            'product_sku_code' => 'dishs',
            'quantity' => 1,
          ),
        ),
        'order_shipping_delivery_tax_rate' => 10.0,
        'order_shipping_delivery_tax_type' => 2,
        'order_shipping_delivery_taxless' => 455.0,
        'order_shipping_delivery_tax' => 45.0,
        'order_shipping_charge_tax_rate' => '10',
        'order_shipping_charge_tax_type' => 2,
        'order_shipping_charge_taxless' => 728.0,
        'order_shipping_charge_tax' => 72.0,
        'delivery_id' => '0000000000000002',
        'order_shipping_delivery_name' => 'クロネコヤマト',
        'order_shipping_delivery_code' => '',
        'order_shipping_delivery_date' => NULL,
        'delivery_time_id' => NULL,
        'order_shipping_delivery_time' => '',
      ),
    ),
    'order_payment' => 
    array (
      'order_payment_charge_tax_rate' => 10.0,
      'order_payment_charge_tax_type' => 2,
      'order_payment_charge_taxless' => 455.0,
      'order_payment_charge_tax' => 45.0,
      'payment_id' => '0000000000000003',
      'payment_method' => '代引き',
      'payment_code' => '',
      'order_payment_corporate_type' => 0,
      'order_payment_type' => '',
    ),
    'order_option' => 
    array (
    ),
    'order_settlement' => 
    array (
    ),
    'order_point' => 
    array (
      'order_point' => 0,
      'order_point_get' => 0,
      'order_point_rate' => '{"point_rate":0,"subscribe_point_rate":0,"rank_point_rate":0}',
      'order_point_use' => 0,
    ),
    'order_coupon' => 
    array (
      'order_coupon' => 0,
      'coupon_id' => '',
      'order_coupon_type' => 0,
      'order_coupon_value' => 0,
      'order_coupon_use' => 0,
      'order_coupon_name' => '',
    ),
  ),
  'area_display' => 
  array (
    'delivery' => true,
    'point' => true,
    'coupon' => true,
    'settlement' => true,
  ),
  'subscribe' => 
  array (
    ... 省略 ...
  ),
)
```

__対象コード）__
- カート情報の編集
    - /app/Http/Controllers/Website/Base/CartControllerBase.php
- 購入情報の編集
    - /app/Http/Controllers/Website/Base/PurchaseControllerBase.php

## エラーハンドリング

発生したエラーのハンドリングは Controller レイヤーで行い、画面遷移等、ユーザへの振る舞いとなるアプリケーションの制御を行います。  
それ以外のレイヤーではハンドリングを行わず、必要に応じて発生したエラーを上位レイヤーへ throw してください。  

発生したエラー種別を Controller で識別する必要がある場合は /common/Exceptions ネームスペースにカスタム Exception を定義します。
なお、エラーに関わるロギングには各レイヤーで行って構いません。

__例）Controllerでのエラーハンドリングによる画面遷移__
``` php
class CustomerController extends CustomerControllerBase
{
    public function postCustomerDetailMailConfirm(Request $request)
    {
        $customerService = new CustomerService();

        try {
            $customerService->checkSaveCustomerDetailMailEdit($request);
        } catch(ValidationException $e) {
            return redirect(Config('const.SYSTEM.WEBMASTER_URL') . '/customer/detail')
                ->withErrors($e->validator);
        }
    }
}
```

## ロギングポリシー

### ログレベル

各環境でのログレベルは下記での運用を想定しています。

- 商用
    - warning
- ステージング
    - info

ログレベルと出力用途の対応は下記とします。

| ログレベル | 利用有無 | 出力用途 |
| --- | --- | --- |
| Log::emergency($message); | ○ | 緊急対応が必要である |
| Log::alert($message); | ○ | 運用対処が必要である |
| Log::critical($message); | × | 使用しない |
| Log::error($message); | ○ | 問題ではあるが、運用対処は必要ない |
| Log::warning($message); | ○ | 問題ではないが改善が必要である |
| Log::notice($message); | × | 使用しない |
| Log::info($message); | ○ | トランザクション情報を追うために必要なキーとなる情報 |
| Log::debug($message); | ○ | 開発環境で参照するデバッグ情報 |

- 参考
    - [Laravel エラーとログ](#https://readouble.com/laravel/6.x/ja/errors.html)

## ユニットテストの実装

理想的には全てのクラスに足してユニットテストを実装しますが、最も効率的だと思われる下記レイヤーのクラスに対しては、ユニットテストの実装を推奨します。  

- Commands
- Repositories
- Services (業務ロジックが含まれる場合のみ)

- 参考
    - [Laravel 6.x データベースのテスト](https://readouble.com/laravel/6.x/ja/database-testing.html)

## マイグレーションの活用（Migration）

EC-Orange は Laravel のマイグレーション機能を利用してスキーマを定義しています。カスタマイズにおいても同様にマイグレーション機能を利用してください。

- マイグレーションファイル格納パス
    - /database/migrations

### ファイルのネーミングルール

マイグレーションファイルは以下のネーミングルールに従って作成してください。

```
{ yyyy_mm_dd_hhmiss }_{ SQLステートメント }_{ テーブル名 }_{ クラス名重複回避用タイムスタンプ }.php
```

- yyyy_mm_dd_hhmiss : マイグレーションファイルの実行順序を明示するためのタイムスタンプ。artisan コマンドで生成した場合に自動で付与される
- SQLステートメント : 該当マイグレーションファイルで実行する SQLステート構文の名称
- テーブル名 : 実行対象のテーブル名
- クラス名重複回避用タイムスタンプ : 同一テーブルへの同一SQLステートメントのマイグレーションファイルがあった場合、PHPのクラス名が重複することを避けるため、クラス名に含めるタイムスタンプ。先頭のタイムスタンプからアンダースコアを除外した文字列とする

__例）__
```
2020_09_08_091305_insert_contact_table_20200908091305.php
```

## Tips

### 実行されるSQLの確認

Repositories にはクエリビルダを利用して検索クエリを実装しまが、複雑なクエリを実装した場合は実際に実行される SQL が自分の意図したものになっているかを確認します。  
下記コードを加えることで実行される SQL がデバッグログに出力されるため、確認がしやすくなります。

__/app/Providers/AppServiceProvider.php__
```php
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

// 省略

public function boot()
{
    DB::listen(function ($query) {
        Log::debug($query->sql);
    });
}
```